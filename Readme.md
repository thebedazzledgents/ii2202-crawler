# Crawler

## Installatin

To install run the following command:

```bash
npm install
```

## Run

To run the project, issue the following command:

```bash
node src/main.js
```

## Database

The script is dependant on a database. See the file `create.sql` for the creation script and modify the values in `src/data-store.js`.