var requestJs = require("request");
var limiter = require("limiter");

var timeToWaitMs = 5 * 1000; // ms
var lamportClock = 0;

var throttler = new limiter.RateLimiter(1, timeToWaitMs);

/**
 * Wrap the requestjs lib and do some boilerplate code
 */
function execute(url, callback) {
    requestJs(url, function (error, response, html) {
        // Make sure to null response if an error occurs
        response = response || null;

        if (response === null || response.statusCode !== 200) {
            console.error("Error downloading page", url, error);
        } else {
            callback(html);
        }
    });
}

module.exports = function (url, callback) {
    var lamportTimestamp = ++lamportClock;

    throttler.removeTokens(1, function(err, remainingRequests) {
        if (err) {
            console.error("Throttle error", err);
        } else {
            var now = new Date();
            console.log("Making request...", {
                lamportClock: lamportTimestamp,
                timestamp: now
            });

            execute(url, callback);
        }
    });
}