/*
* Define how to parse bytes
*/
function parseBytes(html) {
    // Find the last set of numbers. The format will be "16 bytes" or "<s>17</s> 16 bytes".
    // Match that!
    var byteMatches = html.match(/(\d+)/);
    
    // Error checking
    if (byteMatches === null || byteMatches.length <= 0) {
        throw {
            message: "Could not parse bytes",
            input: html
        };
    }

    var bytes = byteMatches[byteMatches.length - 1];
    var bytesInt = parseInt(bytes.trim());
    return bytesInt;
}

module.exports = {
    /**
     * Parse the language when we know there's a link in it
     */
    langLink: function(lang) {
        var langHtml = lang.find("a").first().html();

        // Parse name
        var name = langHtml.trim();
        
        if (name === null || name.length <= 0) {
            throw {
                message: "Link: Could not parse name",
                input: lang.html()
            };
        }

        // Parse bytes
        var bytes = parseBytes(lang.html());

        return {
            name: name,
            bytes: bytes
        };
    },

    /**
     * Parse the language when we know it's plain text.
     *
     * Example input #1: Haskell, 144 bytes
     * Example input #2: Haskell - 144 bytes
     * Example input #3: Haskell, <s>140</s> 144 bytes 
     */
    langText: function (lang) {
        var langHtml = lang.html();

        // Error checking
        if (langHtml === null) {
            throw {
                message: "Text: Could not parse name",
                input: langHtml
            };
        }

        // Parse name
        // var nameMatches = langHtml.match(/^\s*([a-z0-9\(\)\s\-_\+]*[a-z0-9\)]+)\s*[-,].*$/i);
        var nameMatches = langHtml.split(/[,\-\.]/);

        if (nameMatches.length < 2) {
            // Contingency! Lets give it one more try and split it on whitespace
            nameMatches = langHtml.split(/ /);

            if (nameMatches < 2) {
                throw {
                    message: "Text: Could not parse name",
                    input: langHtml
                };
            }
        }

        var name = nameMatches[0].trim();

        // Parse bytes
        var bytes = parseBytes(langHtml);

        return {
            name: name,
            bytes: bytes
        };
    }
};