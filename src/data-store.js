var mysql = require('mysql');

var connection = mysql.createConnection({
    host: 'crawler.cm1iroccuwv8.eu-central-1.rds.amazonaws.com',
    user: 'crawler',
    password: 'xed4uhk6e0HjLKccdibt',
    database: 'crawler'
});

connection.connect();

/**
 * Execute queries within a transaction
 */
function transaction(middle, done) {
    connection.query("START TRANSACTION;", function(err) {
        if (err) {
            throw err;
        }

        middle();

        connection.query("COMMIT;", function(err) {
            if (err) {
                throw err;
            }

            done();
        })
    })
}

module.exports = {
    /**
     * Check if a certain key exists in the data store
     * @param {string} Key of the data
     * @param {function} Callback "function(contains: boolean) {}"
     */
    contains: function (key, callback) {
        // Disable it for now
        callback(false);
        /*
        connection.query('SELECT 1 FROM Question where link = ?', [key], function (err, rows, fields) {
            if (err) {
                throw err;
            }

            // console.log("MySQL returns", rows);

            callback(rows !== null && rows.length > 0);
        });
        */
    },

    /**
     * Insert data into the data store
     * @param {string} Key of the data
     * @param {object} Data to store
     * @param {function} Callback "function(success: boolean, message: string?)"
     */
    insert: function (key, data, callback) {
        transaction(function() {
            connection.query("INSERT IGNORE INTO Question (link) VALUES (?);", [key], function(err) {
                if (err) {
                    throw err;
                }

                connection.query("INSERT IGNORE INTO Language (lang) VALUES (?);", [data.name], function(err) {
                    if (err) {
                        throw err;
                    }

                    connection.query("INSERT INTO Answer(idAnswer, idQuestion, idLanguage, Points) SELECT ?, (SELECT idQuestion FROM Question WHERE link = ? LIMIT 1), (SELECT idLanguage FROM Language WHERE lang = ? LIMIT 1), ? ON DUPLICATE KEY UPDATE idQuestion = VALUES(idQuestion), idLanguage = VALUES(idLanguage), Points = VALUES(Points)", [data.id, key, data.name, data.bytes], function(err) {
                        if (err) {
                            throw err;
                        }
                    });
                });
            })
        }, callback || function() {});
    }
};