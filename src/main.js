var parse = require("./parse");
var cheerio = require("cheerio");
var request = require("./request-wrapper");
var dataStore = require("./data-store");

/*
 * Format of each question:
 *
 * KEY=link to question
 *
 * {
 *      "name": "Java",
 *      "bytes": 179,
 *      "id": 92144
 * }
 */

var domain = "http://codegolf.stackexchange.com";
var pageSize = 50;

/**
 * Entry point for program
 */
function main(page) {
    var pageUrl = "/questions?pagesize=" + pageSize + "&sort=newest&page=" + page;

    // Download the main overview of the questions
    request(domain + pageUrl, function (html) {
        console.info("Page...", pageUrl);

        // Parse the HTML
        var mainPage = cheerio.load(html);

        // Find all the questions
        var questions = mainPage(".question-summary") || [];

        // Exit conditions, no more questions on page
        if (questions.length <= 0) {
            return;
        }

        questions.each(function (index, element) {
            handleQuestions(mainPage, index, element);
        });

        // Get next page
        main(page + 1);
    }); // request
}

// Bootstrap script with first page
main(1);

/**
 * Handle questions
 */
function handleQuestions(mainPage, index, element) {
    var question = mainPage(element);

    // This is the link to the question
    var link = question.find(".question-hyperlink").attr("href");

    // Check if this question already exists in the database
    dataStore.contains(link, function (contains) {
        /*
        if (contains) {
            // Stop execution if we've already handled this question
            return;
        }
        */

        // Lets download it!
        request(domain + link, function (html) {
            console.info("Investigating link...", link);

            // Parse the question and start finding the languages in the answers
            var questionPage = cheerio.load(html);

            // Loop through the answers
            questionPage(".answer").each(function (index, element) {
                handleAnswer(questionPage, link, index, element);
            });
        });
    });

    /*
    // Verify that we actually got answers to parse
    var nAnswersString = question.find(".mini-counts").html() || "0";
    var nAnswers = parseInt(nAnswersString.trim());

    if (nAnswers <= 0) {
        console.log("Has no answers", link);
        return;
    }
    */
}

/**
 * Handle answers
 * @param {object} questionPage The page this answer is listed on
 * @param {string} link URL to the question
 * @param {index} integer The index of this answer in our loop
 * @param {object} element The parent element
 */
function handleAnswer(questionPage, link, index, element) {
    try {
        var answer = questionPage(element);

        var answerId = parseInt(answer.data("answerid"));
        var title = answer.find(".post-text > h2:first-child, .post-text > h1:first-child").first();
        var titleHtml = title.html();

        var result;

        // Check if the first occurence is a link to the programming language
        if (/^\s*\<a/.test(titleHtml)) {
            // Parse according to method #2
            result = parse.langLink(title);
        } else {
            // Parse according to method #1
            result = parse.langText(title);
        }

        result.id = answerId;

        // Insert the data into the data store
        dataStore.insert(link, result);

        console.log("Parsing answer", result);
    } catch (error) {
        console.error(error);
    }
}